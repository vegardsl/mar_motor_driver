# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Motor driver firmware for a mobile autonomous robot.
Project files for Atmel Studio 7.

### How do I get set up? ###

Clone the repository into a folder: mar_motor_driver. Open the project in Atmel studio 7.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact